import logging


class A(object):
    def __init__(self):
        self.num = 5

    def bla(self):
        print("bla")


class Proxy(object):
    def __init__(self, a):
        self.a = a

    def __getattr__(self, attr):
        logging.info("Proxy access")
        return getattr(self.a, attr)


def main():
    logging.basicConfig(filename='PROXY HORROR.log', level=logging.INFO, format='%(asctime)s %(message)s')
    a = A()
    p = Proxy(a)
    print(p.num)
    p.bla()


if __name__ == '__main__':
    main()